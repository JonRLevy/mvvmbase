﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace JonRLevy.MVVMBase.ViewModels
{
    /// <summary>
    /// Generic EditableViewModelBase, that includes a Model. Abstract, as a method to copy a Model to a working copy
    /// (<see cref="makeWorkReplica(TModel)"/> must be provided.
    /// </summary>
    /// <typeparam name="TApp">The Application class the ViewModel is used in.</typeparam>
    /// <typeparam name="TModel">The type of the Model.</typeparam>
    abstract public class EditableViewModelBase<TApp, TModel> : EditableViewModelBase<TApp>
        where TApp: Application
        where TModel: class
    {
        /// <summary>
        /// Creates an EditableViewModelBase by calling the base constructor. 
        /// </summary>
        public EditableViewModelBase() : base()
        { }

        /// <summary>
        /// Creates a EditableViewModelBase with the provided Model. Call the base constructor.
        /// </summary>
        /// <param name="model"></param>
        public EditableViewModelBase(TModel model) : base()
        {
            currentModel = model;
        }

        TModel currentModel;
        TModel workModel = null;

        /// <summary>
        /// Provides access to the encapsulated model. If in editmode provides access to a work copy.
        /// </summary>
        public TModel Model
        {
            get => IsInEditMode ? workModel : currentModel;
            set
            {
                if (IsInEditMode)
                {
                    if (SetProperty(ref workModel, value))
                        IsEdited = true;
                }
                else
                    SetProperty(ref currentModel, value);
            }
        }

        /// <summary>
        /// Must be implemented and make a copy of a model object, that can be worked on in edit mode.
        /// </summary>
        /// <param name="currentModel">The model object to copy.</param>
        /// <returns>The copy to work on.</returns>
        abstract protected TModel makeWorkReplica(TModel currentModel);

        /// <summary>
        /// Creates a copy of the model object to work on, and enters EditMode.
        /// </summary>
        public override void GoToEditMode()
        {
            workModel = makeWorkReplica(currentModel);        
            
            base.GoToEditMode();
        }

        /// <summary>
        /// Sets the model to the working model, exits editmode. Override to provide actual save logic.
        /// </summary>
        public override void SaveEdits()
        {
            currentModel = workModel;
            workModel = null;
            base.SaveEdits();
        }

        /// <summary>
        /// Voids the working model, exits editmode.
        /// </summary>
        public override void AbortEdits()
        {
            workModel = null;
            base.AbortEdits();
        }
    }
}
