﻿using Jon.RLevy.MVVMBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JonRLevy.MVVMBase
{
    /// <summary>
    /// An implementation of the <see cref="IDerivedPropertiesMapper"/> and <see cref="IDependentCommandsMapper"/> interfaces,
    /// providing storage for mappings of derived properties and dependent commands, for tracking when notification events should be invoked.
    /// </summary>
    public class DerivedPropertiesAndDependentCommandsMapper : IDerivedPropertiesMapper, IDependentCommandsMapper
    {
        Dictionary<string, HashSet<string>> derivedPropertiesMap = new Dictionary<string, HashSet<string>>();
        Dictionary<string, HashSet<RelayCommandBase>> dependentCommandsMap = new Dictionary<string, HashSet<RelayCommandBase>>();

        /// <summary>
        /// Add a property mapping, for a derivedProperty derived from a baseProperty
        /// </summary>
        /// <param name="baseProperty">The name of the property the derivedProperty is derived from.</param>
        /// <param name="derivedProperty">The name of the property derived from the baseProperty.</param>
        public void AddDerivedProperty(string baseProperty, string derivedProperty)
        {
            if (!derivedPropertiesMap.ContainsKey(baseProperty))
                derivedPropertiesMap[baseProperty] = new HashSet<string>();

            derivedPropertiesMap[baseProperty].Add(derivedProperty);
        }

        /// <summary>
        /// Add a property mappings, for a derivedProperties derived from a baseProperty
        /// </summary>
        /// <param name="baseProperty">The name of the property the derivedProperties are derived from.</param>
        /// <param name="derivedProperties">An enumeration of the names of the properties derived from the baseProperty.</param>
        public void AddDerivedProperties(string baseProperty, IEnumerable<string> derivedProperties)
        {
            if (!derivedPropertiesMap.ContainsKey(baseProperty))
                derivedPropertiesMap[baseProperty] = new HashSet<string>();

            foreach (string derivedProperty in derivedProperties)
                derivedPropertiesMap[baseProperty].Add(derivedProperty);
        }

        /// <summary>
        /// Add a property mappings, for a derivedProperty derived from a baseProperties
        /// </summary>
        /// <param name="derivedProperty">The name of the property derived from the baseProperties.</param>
        /// <param name="baseProperties">An enumeration of the names of properties the derivedProperty are derived from.</param>
        public void AddBaseProperties(string derivedProperty, IEnumerable<string> baseProperties)
        {
            foreach (string baseProperty in baseProperties)
                AddDerivedProperty(baseProperty, derivedProperty);
        }

        /// <summary>
        /// The implementation of the <see cref="IDerivedPropertiesMapper"/> interface. Returns an enumeration of nammes of properties
        /// derived from the baseProperty (including the baseProperty itself).
        /// </summary>
        /// <param name="baseProperty">The name of the property to return derived names of derived properties for, including itself.</param>
        /// <returns>An enumeartion of the mapped derived properties, including the baseProperty.</returns>
        public IEnumerable<string> GetDerivedProperties(string baseProperty)
        {
            List<string> derivedProperties = new List<string>
            {
                baseProperty
            };
            if (derivedPropertiesMap.ContainsKey(baseProperty))
            {
                foreach (string derivedProperty in derivedPropertiesMap[baseProperty])
                    findPropertyDerivations(derivedProperty, derivedProperties);
            }

            return derivedProperties;
        }

        private void findPropertyDerivations(string baseProperty, List<string> derivedProperties)
        {
            if (derivedProperties.Contains(baseProperty))
                return;

            derivedProperties.Add(baseProperty);
            if (derivedPropertiesMap.ContainsKey(baseProperty))
            {
                foreach (string derivedProperty in derivedPropertiesMap[baseProperty])
                    findPropertyDerivations(derivedProperty, derivedProperties);
            }
        }

        /// <summary>
        /// Add a <see cref="RelayCommandBase"/> command, whose ability to be executed is dependent on a property.
        /// </summary>
        /// <param name="dependentCommand"><see cref="RelayCommandBase"/> command, whose ability to be executed is dependent on propertyName.</param>
        /// <param name="propertyName">The name of the property the dependentCommand is dependent on.</param>
        /// <returns>The dependentCommand</returns>
        public RelayCommandBase AddDependentCommand(RelayCommandBase dependentCommand, string propertyName)
        {
            if (dependentCommand == null)
                throw new ArgumentNullException(nameof(dependentCommand));

            addDependentCommand(dependentCommand, propertyName);

            return dependentCommand;
        }

        /// <summary>
        /// Add a <see cref="RelayCommandBase"/> command, whose ability to be executed is dependent on an enumeration of properties.
        /// </summary>
        /// <param name="dependentCommand"><see cref="RelayCommandBase"/> command, whose ability to be executed is dependent on propertyName.</param>
        /// <param name="propertyNames">The enumeration of names of properties the dependentCommand is dependent on.</param>
        /// <returns>The dependentCommand</returns>
        public RelayCommandBase AddDependentCommand(RelayCommandBase dependentCommand, IEnumerable<string> propertyNames)
        {
            if (dependentCommand == null)
                throw new ArgumentNullException(nameof(dependentCommand));

            if (propertyNames == null)
                throw new ArgumentNullException(nameof(propertyNames));

            foreach (string propertyName in propertyNames)
                addDependentCommand(dependentCommand, propertyName);

            return dependentCommand;
        }

        private void addDependentCommand(RelayCommandBase dependentCommand, string propertyName)
        {
            if (!dependentCommandsMap.ContainsKey(propertyName))
                dependentCommandsMap[propertyName] = new HashSet<RelayCommandBase>();

            dependentCommandsMap[propertyName].Add(dependentCommand);
        }

        /// <summary>
        /// The implementation of <see cref="IDependentCommandsMapper"/>, return an enumeration of <see cref="RelayCommandBase"/>
        /// whose ability to be executed are dependent on the propertyName.
        /// </summary>
        /// <param name="propertyName">The name of the property to find dependentCommands for. </param>
        /// <returns>An enumeration of <see cref="RelayCommandBase"/> whose ability to be executed are dependent on the value of the property named in propertyName.</returns>
        public IEnumerable<RelayCommandBase> GetDependentCommands(string propertyName)
        {
            HashSet<RelayCommandBase> dependentCommands = new HashSet<RelayCommandBase>();

            foreach (string derivedPropertyName in GetDerivedProperties(propertyName))
            {
                if (dependentCommandsMap.ContainsKey(derivedPropertyName))
                {
                    foreach (RelayCommandBase dependentCommand in dependentCommandsMap[derivedPropertyName])
                    {
                        dependentCommands.Add(dependentCommand);
                    }
                }
            }

            return dependentCommands;
        }
    }
}
