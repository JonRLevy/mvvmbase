﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

using static JonRLevy.MVVMBase.Converters.ConverterHelper;

namespace JonRLevy.MVVMBase.Converters
{
    /// <summary>
    /// Converts a value to a Windows.UI.Xaml.Visibility value. By first converting to a bool using the 
    /// ConverterHelper.onvertToBool and from that to the Visibility value as per
    /// the ConverterHelper.BoolToVisibility method. If IsReversed is set to True the boolean will be reversed
    /// before converting, meaning false becomes Visibility.Visible and true becomes Visibility.Collapsed.
    /// </summary>
    public class ToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Set this property to true to reverse the boolean to visibility-conversion.
        /// </summary>
        public bool IsReversed { get; set; }

        /// <summary>
        /// Converts a value to a Windows.UI.Xaml.Visibility value. By first converting to a bool using the 
        /// ConverterHelper.onvertToBool and from that to the Visibility value as per
        /// the ConverterHelper.BoolToVisibility method. If IsReversed is set to True the boolean will be reversed
        /// before converting, meaning false becomes Visibility.Visible and true becomes Visibility.Collapsed.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="targetType">Not used (but will be a Windows.UI.Xaml.Visibility)</param>
        /// <param name="parameter">Not used</param>
        /// <param name="language">Not used</param>
        /// <returns>The converted value; will either be a Visibility.Collapsed or a Visibility.Visible value.</returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            bool b = ConvertToBool(value);
            return IsReversed ? BoolToVisibility(!b) : BoolToVisibility(b);
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="value">Not used</param>
        /// <param name="targetType">Not used</param>
        /// <param name="parameter">Not used</param>
        /// <param name="language">Not used</param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

}
