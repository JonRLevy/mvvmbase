﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace JonRLevy.MVVMBase.Converters
{
    /// <summary>
    /// Static methods for general use by Converters.
    /// </summary>
    public static class ConverterHelper
    {
        /// <summary>
        /// Convert an object to bool; first attempts to use the System.Convert method; if this fails, i.e value
        /// cannot be seen as a bool value, it falls back to whether the value is null or not; null equals false, non-null
        /// equals true.
        /// </summary>
        /// <param name="value">The object to convert to a boolean.</param>
        /// <returns>The converted value.</returns>
        public static bool ConvertToBool(object value)
        {
            try
            {
                return System.Convert.ToBoolean(value);
            }
            catch (Exception)
            {                
                return value!=null;
            }
        }
         
        /// <summary>
        /// Converts a bool value to a Windows.UI.Xaml.Visibility value; false=collapsed, true=Visible
        /// </summary>
        /// <param name="b">The boolean to convert</param>
        /// <returns>The converted value; either Visibility.Collapsed (false) or Visibility.Visible (true)</returns>
        public static Visibility BoolToVisibility(bool b)
            => b ? Visibility.Visible : Visibility.Collapsed;
    }
}
