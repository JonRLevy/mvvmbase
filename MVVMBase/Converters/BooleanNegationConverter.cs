﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using static JonRLevy.MVVMBase.Converters.ConverterHelper;

namespace JonRLevy.MVVMBase.Converters
{
    /// <summary>
    /// Converter that simply returns the reversed Boolean value of the value parameter (converted from an object using the
    /// ConverterHelper.ConvertToBool method.
    /// </summary>
    public class BooleanNegationConverter : IValueConverter
    {
        /// <summary>
        /// Returns the reversed Boolean value of the value parameter(converted from an object using the
        /// ConverterHelper.ConvertToBool method.        
        /// </summary>
        /// <param name="value">The object to convert to it's reverse boolean</param>
        /// <param name="targetType">Not used (but return type will be a bool</param>
        /// <param name="parameter">Not used</param>
        /// <param name="language">Not used</param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, string language)
            => !ConvertToBool(value);

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="value">Not implemented</param>
        /// <param name="targetType">Not implemented</param>
        /// <param name="parameter">Not implemented</param>
        /// <param name="language">Not implemented</param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
