﻿using Jon.RLevy.MVVMBase;
using JonRLevy.MVVMBase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MVVMBaseTests
{
    [TestClass]
    public class DerivedPropertiesMapperTests
    {
        bool testCommand1_CanExecute()
        {
            return true;
        }

        void testCommand1_Execute()
        {
        }

        private RelayCommand testCommand1;
        public ICommand TestCommand1 => testCommand1 ?? (testCommand1 = new RelayCommand(testCommand1_Execute, testCommand1_CanExecute));


        bool testCommand2_CanExecute()
        {
            return true;
        }

        void testCommand2_Execute()
        {
        }

        private RelayCommand testCommand2;
        public ICommand TestCommand2 => testCommand2 ?? (testCommand2 = new RelayCommand(testCommand2_Execute, testCommand2_CanExecute));

        [TestMethod]
        public void DerivedPropertiesMapperEmptyTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();

            List<string> resultList = dpm.GetDerivedProperties("TestProperty1").ToList();
            Assert.AreEqual(1, resultList.Count);
            Assert.AreEqual("TestProperty1", resultList[0]);
        }

        [TestMethod]
        public void DerivedPropertiesMapperOneDerivedPropertyTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDerivedProperty("TestProperty1", "TestProperty2");

            List<string> resultList1 = dpm.GetDerivedProperties("TestProperty1").OrderBy(x => x).ToList();
            Assert.AreEqual(2, resultList1.Count);
            Assert.AreEqual("TestProperty1", resultList1[0]);
            Assert.AreEqual("TestProperty2", resultList1[1]);

            List<string> resultList2 = dpm.GetDerivedProperties("TestProperty2").OrderBy(x => x).ToList();
            Assert.AreEqual(1, resultList2.Count);
            Assert.AreEqual("TestProperty2", resultList2[0]);
        }

        [TestMethod]
        public void DerivedPropertiesMapperThreeDerivedPropertyWithRingTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDerivedProperty("TestProperty1", "TestProperty2");
            dpm.AddDerivedProperty("TestProperty1", "TestProperty3");
            dpm.AddDerivedProperty("TestProperty2", "TestProperty3");
            dpm.AddDerivedProperty("TestProperty3", "TestProperty4");
            dpm.AddDerivedProperty("TestProperty3", "TestProperty1");

            List<string> resultList1 = dpm.GetDerivedProperties("TestProperty1").OrderBy(x => x).ToList();
            Assert.AreEqual(4, resultList1.Count);
            Assert.AreEqual("TestProperty1", resultList1[0]);
            Assert.AreEqual("TestProperty2", resultList1[1]);
            Assert.AreEqual("TestProperty3", resultList1[2]);
            Assert.AreEqual("TestProperty4", resultList1[3]);

            List<string> resultList2 = dpm.GetDerivedProperties("TestProperty2").OrderBy(x => x).ToList();
            Assert.AreEqual(4, resultList2.Count);
            Assert.AreEqual("TestProperty1", resultList2[0]);
            Assert.AreEqual("TestProperty2", resultList2[1]);
            Assert.AreEqual("TestProperty3", resultList2[2]);
            Assert.AreEqual("TestProperty4", resultList2[3]);

            List<string> resultList3 = dpm.GetDerivedProperties("TestProperty3").OrderBy(x => x).ToList();
            Assert.AreEqual(4, resultList3.Count);
            Assert.AreEqual("TestProperty1", resultList3[0]);
            Assert.AreEqual("TestProperty2", resultList3[1]);
            Assert.AreEqual("TestProperty3", resultList3[2]);
            Assert.AreEqual("TestProperty4", resultList3[3]);

            List<string> resultList4 = dpm.GetDerivedProperties("TestProperty4").OrderBy(x => x).ToList();
            Assert.AreEqual(1, resultList4.Count);
            Assert.AreEqual("TestProperty4", resultList4[0]);
        }

        [TestMethod]
        public void DerivedPropertiesMapperThreeDerivedPropertyWithNoRingTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDerivedProperty("TestProperty1", "TestProperty2");
            dpm.AddDerivedProperty("TestProperty1", "TestProperty3");
            dpm.AddDerivedProperty("TestProperty2", "TestProperty3");
            dpm.AddDerivedProperty("TestProperty3", "TestProperty4");

            List<string> resultList1 = dpm.GetDerivedProperties("TestProperty1").OrderBy(x => x).ToList();
            Assert.AreEqual(4, resultList1.Count);
            Assert.AreEqual("TestProperty1", resultList1[0]);
            Assert.AreEqual("TestProperty2", resultList1[1]);
            Assert.AreEqual("TestProperty3", resultList1[2]);
            Assert.AreEqual("TestProperty4", resultList1[3]);

            List<string> resultList2 = dpm.GetDerivedProperties("TestProperty2").OrderBy(x => x).ToList();
            Assert.AreEqual(3, resultList2.Count);
            Assert.AreEqual("TestProperty2", resultList2[0]);
            Assert.AreEqual("TestProperty3", resultList2[1]);
            Assert.AreEqual("TestProperty4", resultList2[2]);

            List<string> resultList3 = dpm.GetDerivedProperties("TestProperty3").OrderBy(x => x).ToList();
            Assert.AreEqual(2, resultList3.Count);
            Assert.AreEqual("TestProperty3", resultList3[0]);
            Assert.AreEqual("TestProperty4", resultList3[1]);

            List<string> resultList4 = dpm.GetDerivedProperties("TestProperty4").OrderBy(x => x).ToList();
            Assert.AreEqual(1, resultList4.Count);
            Assert.AreEqual("TestProperty4", resultList4[0]);
        }

        [TestMethod]
        public void DerivedPropertiesMapperAddDerivedListTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDerivedProperties("TestProperty1", new List<string>() { "TestProperty2", "TestProperty3", "TestProperty4" });

            List<string> resultList1 = dpm.GetDerivedProperties("TestProperty1").OrderBy(x => x).ToList();
            Assert.AreEqual(4, resultList1.Count);
            Assert.AreEqual("TestProperty1", resultList1[0]);
            Assert.AreEqual("TestProperty2", resultList1[1]);
            Assert.AreEqual("TestProperty3", resultList1[2]);
            Assert.AreEqual("TestProperty4", resultList1[3]);

            List<string> resultList2 = dpm.GetDerivedProperties("TestProperty2").OrderBy(x => x).ToList();
            Assert.AreEqual(1, resultList2.Count);
            Assert.AreEqual("TestProperty2", resultList2[0]);

            List<string> resultList3 = dpm.GetDerivedProperties("TestProperty3").OrderBy(x => x).ToList();
            Assert.AreEqual(1, resultList3.Count);
            Assert.AreEqual("TestProperty3", resultList3[0]);

            List<string> resultList4 = dpm.GetDerivedProperties("TestProperty4").OrderBy(x => x).ToList();
            Assert.AreEqual(1, resultList4.Count);
            Assert.AreEqual("TestProperty4", resultList4[0]);
        }

        [TestMethod]
        public void DerivedPropertiesMapperAddBaseListTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddBaseProperties("TestProperty1", new List<string>() { "TestProperty2", "TestProperty3", "TestProperty4" });

            List<string> resultList1 = dpm.GetDerivedProperties("TestProperty1").OrderBy(x => x).ToList();
            Assert.AreEqual(1, resultList1.Count);
            Assert.AreEqual("TestProperty1", resultList1[0]);

            List<string> resultList2 = dpm.GetDerivedProperties("TestProperty2").OrderBy(x => x).ToList();
            Assert.AreEqual(2, resultList2.Count);
            Assert.AreEqual("TestProperty1", resultList2[0]);
            Assert.AreEqual("TestProperty2", resultList2[1]);

            List<string> resultList3 = dpm.GetDerivedProperties("TestProperty3").OrderBy(x => x).ToList();
            Assert.AreEqual(2, resultList3.Count);
            Assert.AreEqual("TestProperty1", resultList3[0]);
            Assert.AreEqual("TestProperty3", resultList3[1]);

            List<string> resultList4 = dpm.GetDerivedProperties("TestProperty4").OrderBy(x => x).ToList();
            Assert.AreEqual(2, resultList4.Count);
            Assert.AreEqual("TestProperty1", resultList4[0]);
            Assert.AreEqual("TestProperty4", resultList4[1]);
        }

        [TestMethod]
        public void DependentCommandsMapperEmptyTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();

            List<RelayCommandBase> resultList = dpm.GetDependentCommands("TestProperty1").ToList();
            Assert.AreEqual(0, resultList.Count);
        }

        [TestMethod]
        public void DependentCommandsMapperOneDependentCommandTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty1");

            List<RelayCommandBase> resultList1 = dpm.GetDependentCommands("TestProperty1").ToList();
            Assert.AreEqual(1, resultList1.Count);
            Assert.AreSame(testCommand1, resultList1[0]);

            List<RelayCommandBase> resultList2 = dpm.GetDependentCommands("TestProperty2").ToList();
            Assert.AreEqual(0, resultList2.Count);
        }

        [TestMethod]
        public void DependentCommandsMapperTwoDependentCommandTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty1");
            dpm.AddDependentCommand(TestCommand2 as RelayCommand, "TestProperty1");

            List<RelayCommandBase> resultList1 = dpm.GetDependentCommands("TestProperty1").ToList();
            Assert.AreEqual(2, resultList1.Count);
            Assert.AreSame(testCommand1, resultList1[0]);
            Assert.AreSame(testCommand2, resultList1[1]);

            List<RelayCommandBase> resultList2 = dpm.GetDependentCommands("TestProperty2").ToList();
            Assert.AreEqual(0, resultList2.Count);
        }

        [TestMethod]
        public void DependentCommandsMapperDependentCommandTwoPropertiesTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty1");
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty2");

            List<RelayCommandBase> resultList1 = dpm.GetDependentCommands("TestProperty1").ToList();
            Assert.AreEqual(1, resultList1.Count);
            Assert.AreSame(testCommand1, resultList1[0]);

            List<RelayCommandBase> resultList2 = dpm.GetDependentCommands("TestProperty2").ToList();
            Assert.AreEqual(1, resultList2.Count);
            Assert.AreSame(testCommand1, resultList2[0]);
        }

        [TestMethod]
        public void DependentCommandsMapperDependentCommandTwoPropertiesAsListTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, new List<string> { "TestProperty1", "TestProperty2" });

            List<RelayCommandBase> resultList1 = dpm.GetDependentCommands("TestProperty1").ToList();
            Assert.AreEqual(1, resultList1.Count);
            Assert.AreSame(testCommand1, resultList1[0]);

            List<RelayCommandBase> resultList2 = dpm.GetDependentCommands("TestProperty2").ToList();
            Assert.AreEqual(1, resultList2.Count);
            Assert.AreSame(testCommand1, resultList2[0]);
        }

        [TestMethod]
        public void DependentCommandsMapperDependentCommandOnDerivedPropertyTest()
        {
            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();

            dpm.AddDerivedProperty("TestProperty1", "TestProperty2");
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty2");

            List<RelayCommandBase> resultList1 = dpm.GetDependentCommands("TestProperty1").ToList();
            Assert.AreEqual(1, resultList1.Count);
            Assert.AreSame(testCommand1, resultList1[0]);

            List<RelayCommandBase> resultList2 = dpm.GetDependentCommands("TestProperty2").ToList();
            Assert.AreEqual(1, resultList2.Count);
            Assert.AreSame(testCommand1, resultList2[0]);
        }
    }
}
