﻿using Jon.RLevy.MVVMBase;
using JonRLevy.MVVMBase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MVVMBaseTests
{
    [TestClass]
    public class BindableBaseTests : BindableBase
    {
        bool testCommand1_CanExecute()
        {
            return true;
        }

        void testCommand1_Execute()
        {
        }

        private RelayCommand testCommand1;
        public ICommand TestCommand1 => testCommand1 ?? (testCommand1 = new RelayCommand(testCommand1_Execute, testCommand1_CanExecute));

        bool testCommand2_CanExecute()
        {
            return true;
        }

        void testCommand2_Execute()
        {
        }

        private RelayCommand testCommand2;
        public ICommand TestCommand2 => testCommand2 ?? (testCommand2 = new RelayCommand(testCommand2_Execute, testCommand2_CanExecute));

        int TestCommandCanExecuteChangedRaisedCount = 0;
        void TestCommandCanExecuteChangedEventHandler(object s, EventArgs a)
            => TestCommandCanExecuteChangedRaisedCount++;

        private int testProperty;
        public int TestProperty
        {
            get => testProperty;
            set
            {
                testPropertyWasChanged=SetProperty(ref testProperty, value);
            }
        }

        bool testPropertyWasChanged;
        string onPropertyChangedMessage;
        void propertyChangedEventHandler(object sender, PropertyChangedEventArgs args)
            => onPropertyChangedMessage = args.PropertyName;

        List<string> onPropertyChangedMessageList;
        void propertyChangedEventHandlerToList(object sender, PropertyChangedEventArgs args)
            => onPropertyChangedMessageList.Add(args.PropertyName);

        [TestMethod]
        public void SetPropertyTest()
        {
            testProperty = 0;
            testPropertyWasChanged = false;

            TestProperty = 0;
            Assert.AreEqual(false, testPropertyWasChanged);
            Assert.AreEqual(0, testProperty);

            TestProperty = 50;
            Assert.AreEqual(true, testPropertyWasChanged);
            Assert.AreEqual(50, testProperty);
        }

        [TestMethod]
        public void PropertyChangedEventTest()
        {
            testProperty = 0;
            testPropertyWasChanged = false;
            onPropertyChangedMessage = "";

            TestProperty = 50;
            Assert.AreEqual("", onPropertyChangedMessage);

            PropertyChanged += propertyChangedEventHandler;
            TestProperty = 0;
            Assert.AreEqual("TestProperty", onPropertyChangedMessage);

            onPropertyChangedMessage = "";
            TestProperty = 0;
            Assert.AreEqual("", onPropertyChangedMessage);

            PropertyChanged -= propertyChangedEventHandler;
        }

        [TestMethod]
        public void OnPropertyChangedTest()
        {
            onPropertyChangedMessage = "";

            OnPropertyChanged("Hello");
            Assert.AreEqual("", onPropertyChangedMessage);

            PropertyChanged += propertyChangedEventHandler;
            OnPropertyChanged("Hello");
            Assert.AreEqual("Hello", onPropertyChangedMessage);

            PropertyChanged -= propertyChangedEventHandler;
        }

        [TestMethod]
        public void PropertyChangedWitDerivedPropertiesEventTest()
        {
            testProperty = 0;
            testPropertyWasChanged = false;
            onPropertyChangedMessageList = new List<string>();

            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDerivedProperty("TestProperty", "TestProperty2");
            dpm.AddDerivedProperty("TestProperty", "TestProperty3");
            dpm.AddDerivedProperty("TestProperty2", "TestProperty3");
            dpm.AddDerivedProperty("TestProperty3", "TestProperty4");

            this.DerivedPropertiesMapper = dpm;

            TestProperty = 50;
            Assert.AreEqual(0, onPropertyChangedMessageList.Count);

            PropertyChanged += propertyChangedEventHandlerToList;
            TestProperty = 0;

            List<string> resultList1 = onPropertyChangedMessageList.OrderBy(x => x).ToList();
            Assert.AreEqual(4, resultList1.Count);
            Assert.AreEqual("TestProperty", resultList1[0]);
            Assert.AreEqual("TestProperty2", resultList1[1]);
            Assert.AreEqual("TestProperty3", resultList1[2]);
            Assert.AreEqual("TestProperty4", resultList1[3]);

            PropertyChanged -= propertyChangedEventHandlerToList;
        }

        [TestMethod]
        public void PropertyChangedWithDependentCommandsEventTest()
        {
            testProperty = 0;
            testPropertyWasChanged = false;
            TestCommandCanExecuteChangedRaisedCount = 0;
            onPropertyChangedMessageList = new List<string>();

            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDerivedProperty("TestProperty", "TestProperty2");
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty2");
            DerivedPropertiesMapper = dpm;
            DependentCommandsMapper = dpm;

            TestProperty = 50;
            Assert.AreEqual(0, onPropertyChangedMessageList.Count);
            Assert.AreEqual(0, TestCommandCanExecuteChangedRaisedCount);

            TestCommand1.CanExecuteChanged += TestCommandCanExecuteChangedEventHandler;
            PropertyChanged += propertyChangedEventHandlerToList;
            TestProperty = 0;

            List<string> resultList1 = onPropertyChangedMessageList.OrderBy(x => x).ToList();
            Assert.AreEqual(2, resultList1.Count);
            Assert.AreEqual("TestProperty", resultList1[0]);
            Assert.AreEqual("TestProperty2", resultList1[1]);
            Assert.AreEqual(1, TestCommandCanExecuteChangedRaisedCount);
            TestCommandCanExecuteChangedRaisedCount = 0;

            PropertyChanged -= propertyChangedEventHandlerToList;
            TestCommand1.CanExecuteChanged -= TestCommandCanExecuteChangedEventHandler;
        }

        [TestMethod]
        public void TwoPropertyChangesWithDependentCommandsEventTest()
        {
            testProperty = 0;
            testPropertyWasChanged = false;
            TestCommandCanExecuteChangedRaisedCount = 0;
            onPropertyChangedMessageList = new List<string>();

            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDerivedProperty("TestProperty", "TestProperty2");
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty");
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty2");
            DerivedPropertiesMapper = dpm;
            DependentCommandsMapper = dpm;

            TestProperty = 50;
            Assert.AreEqual(0, onPropertyChangedMessageList.Count);
            Assert.AreEqual(0, TestCommandCanExecuteChangedRaisedCount);

            TestCommand1.CanExecuteChanged += TestCommandCanExecuteChangedEventHandler;
            PropertyChanged += propertyChangedEventHandlerToList;
            TestProperty = 0;

            List<string> resultList1 = onPropertyChangedMessageList.OrderBy(x => x).ToList();
            Assert.AreEqual(2, resultList1.Count);
            Assert.AreEqual("TestProperty", resultList1[0]);
            Assert.AreEqual("TestProperty2", resultList1[1]);
            Assert.AreEqual(1, TestCommandCanExecuteChangedRaisedCount);
            TestCommandCanExecuteChangedRaisedCount = 0;

            PropertyChanged -= propertyChangedEventHandlerToList;
            TestCommand1.CanExecuteChanged -= TestCommandCanExecuteChangedEventHandler;
        }

        [TestMethod]
        public void TwoPropertyChangesWithTwoDependentCommandsEventTest()
        {
            testProperty = 0;
            testPropertyWasChanged = false;
            TestCommandCanExecuteChangedRaisedCount = 0;
            onPropertyChangedMessageList = new List<string>();

            DerivedPropertiesAndDependentCommandsMapper dpm = new DerivedPropertiesAndDependentCommandsMapper();
            dpm.AddDerivedProperty("TestProperty", "TestProperty2");
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty");
            dpm.AddDependentCommand(TestCommand1 as RelayCommand, "TestProperty2");
            dpm.AddDependentCommand(TestCommand2 as RelayCommand, "TestProperty2");
            DerivedPropertiesMapper = dpm;
            DependentCommandsMapper = dpm;

            TestProperty = 50;
            Assert.AreEqual(0, onPropertyChangedMessageList.Count);
            Assert.AreEqual(0, TestCommandCanExecuteChangedRaisedCount);

            TestCommand1.CanExecuteChanged += TestCommandCanExecuteChangedEventHandler;
            TestCommand2.CanExecuteChanged += TestCommandCanExecuteChangedEventHandler;
            PropertyChanged += propertyChangedEventHandlerToList;
            TestProperty = 0;

            List<string> resultList1 = onPropertyChangedMessageList.OrderBy(x => x).ToList();
            Assert.AreEqual(2, resultList1.Count);
            Assert.AreEqual("TestProperty", resultList1[0]);
            Assert.AreEqual("TestProperty2", resultList1[1]);
            Assert.AreEqual(2, TestCommandCanExecuteChangedRaisedCount);
            TestCommandCanExecuteChangedRaisedCount = 0;

            PropertyChanged -= propertyChangedEventHandlerToList;
            TestCommand1.CanExecuteChanged -= TestCommandCanExecuteChangedEventHandler;
            TestCommand2.CanExecuteChanged -= TestCommandCanExecuteChangedEventHandler;
        }
    }
}
