﻿using JonRLevy.MVVMBase.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMBaseTests
{
    [TestClass]
    public class ViewModelBase1Tests : ViewModelBase<App>
    {
        [TestMethod]
        public void ViewModelBase1TestApp()
        {
            string s1 = App.HelloString;

            Assert.AreEqual("HelloString", s1);
        }

        [TestMethod]
        public void ViewModelBase1TestInDesignMode()
        {
            Assert.AreEqual(false, IsInDesignMode);
        }
    }
}
