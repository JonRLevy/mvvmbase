﻿using JonRLevy.MVVMBase.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMBaseTests
{
    public class TestModel
    {
        public string DummyString { get; set; }
    }

    [TestClass]
    public class ViewModelBase2Tests 
    {
        ViewModelBase<App, TestModel> testViewModel;

        [TestMethod]
        public void ViewModelBase2TestApp()
        {
            TestModel tm = new TestModel { DummyString = "Hello" };
            testViewModel = new ViewModelBase<App, TestModel>(tm);

            string s1 = testViewModel.App.HelloString;
            Assert.AreEqual("HelloString", s1);
        }

        [TestMethod]
        public void ViewModelBase2TestInDesignMode()
        {
            TestModel tm = new TestModel { DummyString = "Hello" };
            testViewModel = new ViewModelBase<App, TestModel>(tm);

            Assert.AreEqual(false, testViewModel.IsInDesignMode);
        }

        [TestMethod]
        public void ViewModelBase2TestModel()
        {
            TestModel tm = new TestModel { DummyString = "Hello" };
            testViewModel = new ViewModelBase<App, TestModel>(tm);

            Assert.AreEqual("Hello", testViewModel.Model.DummyString);
        }

    }
}
