﻿
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using JonRLevy.MVVMBase.Converters;
using static JonRLevy.MVVMBase.Converters.ConverterHelper;
using Windows.UI.Xaml;

namespace MVVMBaseTests
{
    [TestClass]
    public class ConverterTests
    {
        [TestMethod]
        public void ConverterHelperTests()
        {
            bool result1t = ConvertToBool(true);
            Assert.AreEqual(true, result1t);
            bool result1f = ConvertToBool(false);
            Assert.AreEqual(false, result1f);

            bool result2t = ConvertToBool(10);
            Assert.AreEqual(true, result2t);
            bool result2f = ConvertToBool(0);
            Assert.AreEqual(false, result2f);

            bool result3t = ConvertToBool("true");
            Assert.AreEqual(true, result3t);
            bool result3f = ConvertToBool("false");
            Assert.AreEqual(false, result3f);

            bool result4f1 = ConvertToBool(new object());
            Assert.AreEqual(true, result4f1);
            bool result4f2 = ConvertToBool(null);
            Assert.AreEqual(false, result4f2);

            Visibility resultv1 = BoolToVisibility(false);
            Assert.AreEqual(Visibility.Collapsed, resultv1);
            Visibility resultv2 = BoolToVisibility(true);
            Assert.AreEqual(Visibility.Visible, resultv2);
        }

        [TestMethod]
        public void ToVisibilityConverterTest()
        {
            ToVisibilityConverter tvc1 = new ToVisibilityConverter();
            tvc1.IsReversed = false;

            Visibility resultv1 = (Visibility)tvc1.Convert(true, null, null, null);
            Assert.AreEqual(Visibility.Visible, resultv1);

            tvc1.IsReversed = true;
            Visibility resultv2 = (Visibility)tvc1.Convert(true, null, null, null);
            Assert.AreEqual(Visibility.Collapsed, resultv2);
        }

        [TestMethod]
        public void BooleanNegationConverterTest()
        {
            BooleanNegationConverter bnc = new BooleanNegationConverter();

            bool result1= (bool)bnc.Convert("true", null, null, null);
            Assert.AreEqual(false, result1);

            bool result2 = (bool)bnc.Convert(0, null, null, null);
            Assert.AreEqual(true, result2);
        }


        [TestMethod]
        public void StringFormatConverterTest()
        {
            StringFormatConverter sfc = new StringFormatConverter();

            string result1 = (string)sfc.Convert("string", null, null, null);
            Assert.AreEqual("string", result1);

            string result2 = (string)sfc.Convert("string", null, "format {0} result", null);
            Assert.AreEqual("format string result", result2);

            sfc.FormatString = "hello {0}";
            string result3 = (string)sfc.Convert("string", null, null, null);
            Assert.AreEqual("hello string", result3);

            string result4 = (string)sfc.Convert("string", null, "format {0} result", null);
            Assert.AreEqual("format string result", result4);
        }
    }

}
