﻿using Jon.RLevy.MVVMBase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMBaseTests
{
    [TestClass]
    public class RelayCommanderTests
    {
        bool canExecuteCommandTrue() => true;
        bool canExecuteCommandFalse() => false;

        int modifyInt = 1;
        void executeCommand()
        {
            modifyInt *= 2;
        }

        bool canExecuteCommand(int i) => i != 0;

        void executeCommand(int i)
        {
            modifyInt *= i;
        }

        bool wasRaised = false;
        void canExecuteChanged(object sender, EventArgs e)
        {
            wasRaised = true;
        }

        [TestMethod]
        public void RelayCommandExecuteCommandTest()
        {
            RelayCommand rc = new RelayCommand(executeCommand);

            Assert.AreEqual(true, rc.CanExecute());

            int prevInt = modifyInt;
            rc.Execute();
            Assert.AreEqual(prevInt * 2, modifyInt);
        }

        [TestMethod]
        public void RelayCommandCanExecuteTest()
        {
            RelayCommand rcCannot = new RelayCommand(executeCommand, canExecuteCommandFalse);
            Assert.AreEqual(false, rcCannot.CanExecute());

            RelayCommand rcCan = new RelayCommand(executeCommand, canExecuteCommandTrue);
            Assert.AreEqual(true, rcCan.CanExecute());

            int prevInt = modifyInt;
            rcCan.Execute();
            Assert.AreEqual(prevInt * 2, modifyInt);
        }

        [TestMethod]
        public void RelayCommandRaiseExecuteTest()
        {
            RelayCommand rcNoEvent = new RelayCommand(executeCommand);
            wasRaised = false;
            rcNoEvent.RaiseCanExecuteChanged();
            Assert.AreEqual(false, wasRaised);

            RelayCommand rcEvent = new RelayCommand(executeCommand);
            rcEvent.CanExecuteChanged += canExecuteChanged;
            wasRaised = false;
            rcEvent.RaiseCanExecuteChanged();
            Assert.AreEqual(true, wasRaised);
            rcEvent.CanExecuteChanged -= canExecuteChanged;
        }

        [TestMethod]
        public void RelayCommandIntExecuteCommandTest()
        {
            RelayCommand<int> rc = new RelayCommand<int>(executeCommand);
            int testInt = 3;
            Assert.AreEqual(true, rc.CanExecute(testInt));

            int prevInt = modifyInt;
            rc.Execute(testInt);
            Assert.AreEqual(prevInt * testInt, modifyInt);
        }

        [TestMethod]
        public void RelayCommandIntCanExecuteTest()
        {
            RelayCommand<int> rc = new RelayCommand<int>(executeCommand, canExecuteCommand);
            Assert.AreEqual(false, rc.CanExecute(0));

            int testInt = 4;
            Assert.AreEqual(true, rc.CanExecute(testInt));

            int prevInt = modifyInt;
            rc.Execute(testInt);
            Assert.AreEqual(prevInt * testInt, modifyInt);
        }

        [TestMethod]
        public void RelayCommandIntRaiseExecuteTest()
        {
            RelayCommand<int> rcNoEvent = new RelayCommand<int>(executeCommand);
            wasRaised = false;
            rcNoEvent.RaiseCanExecuteChanged();
            Assert.AreEqual(false, wasRaised);

            RelayCommand<int> rcEvent = new RelayCommand<int>(executeCommand);
            rcEvent.CanExecuteChanged += canExecuteChanged;
            wasRaised = false;
            rcEvent.RaiseCanExecuteChanged();
            Assert.AreEqual(true, wasRaised);
            rcEvent.CanExecuteChanged -= canExecuteChanged;
        }
    }
}
