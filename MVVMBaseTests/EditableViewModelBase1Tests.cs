﻿using JonRLevy.MVVMBase.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMBaseTests
{
    [TestClass]
    public class EditableViewModelBase1Tests
    {
        [TestMethod]
        public void EditableViewModel1AppTest()
        {
            EditableViewModelBase<App> testViewModel = new EditableViewModelBase<App>();
            Assert.AreEqual("HelloString", testViewModel.App.HelloString);
        }

        [TestMethod]
        public void EditableViewModel1DetermineCanEnterEditModeTest()
        {
            EditableViewModelBase<App> testViewModel = new EditableViewModelBase<App>();
            testViewModel.IsInEditMode = false;
            Assert.AreEqual(true, testViewModel.DetermineCanEnterEditMode());

            testViewModel.IsInEditMode = true;
            Assert.AreEqual(false, testViewModel.DetermineCanEnterEditMode());
        }
    }
}
